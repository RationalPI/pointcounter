macro(BuildOpenCV buildType sourceDir buildDir)
    #${CMAKE_BUILD_TYPE}
    set(realBuildDir ${buildDir}/${buildType})

    set(CV_Cmake_Args
        "-G${CMAKE_GENERATOR}"
        -DCMAKE_C_COMPILER=${CMAKE_C_COMPILER}
        -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
        -DCMAKE_BUILD_TYPE=${buildType}
        -DCMAKE_INSTALL_PREFIX=${realBuildDir}/install
        -DCMAKE_PREFIX_PATH=${realBuildDir}/install

        -DOPENCV_EXTRA_MODULES_PATH=${sourceDir}/../opencv_contrib-4.2.0/modules
        -DBUILD_LIST=core,highgui,imgcodecs,imgproc,calib3d,features2d,flann,ml,xfeatures2d
        -DOPENCV_ENABLE_NONFREE=ON

        -DBUILD_CUDA_STUBS=OFF
        -DBUILD_DOCS=OFF
        -DBUILD_EXAMPLES=OFF
        -DBUILD_IPP_IW=OFF
        -DBUILD_ITT=OFF
        -DBUILD_JASPER=OFF
        -DBUILD_JAVA=OFF
        -DBUILD_JPEG=OFF
        -DBUILD_OPENEXR=OFF
        -DBUILD_PACKAGE=OFF
        -DBUILD_PERF_TESTS=OFF
        -DBUILD_PNG=OFF
        -DBUILD_PROTOBUF=OFF
        -DBUILD_SHARED_LIBS=ON
        -DBUILD_TBB=OFF
        -DBUILD_TESTS=OFF
        -DBUILD_TIFF=OFF
        -DBUILD_USE_SYMLINKS=OFF
        -DBUILD_WEBP=OFF
        -DBUILD_WITH_DEBUG_INFO=OFF
        -DBUILD_WITH_DYNAMIC_IPP=OFF
        -DBUILD_ZLIB=OFF
        )

    if(TRUE)#NOT EXISTS ${realBuildDir})
        make_directory(${realBuildDir})
        message("############## Generating OpenCV project into ${realBuildDir} ##############")
        execute_process(COMMAND ${CMAKE_COMMAND} -B ${realBuildDir} ${sourceDir} ${CV_Cmake_Args})
        message("############## building OpenCV into ${realBuildDir} ##############")
        execute_process(COMMAND ${CMAKE_COMMAND} --build ${realBuildDir} --target install --config ${buildType} -j4)
    endif()

    SET("OpenCV_DIR" "${realBuildDir}/install")
    find_package( OpenCV REQUIRED )

    message("")
    message("Available OpenCV libs in \${OpenCV_LIBS}:")
    foreach(lib ${OpenCV_LIBS})
        message(" - ${lib}")
    endforeach()
    message("")

endmacro()

