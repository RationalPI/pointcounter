#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>

void aze(cv::Mat ref, cv::Mat scene);

cv::Mat CropToBigestEllipse(const cv::Mat& src){
	/*aze implement*/
	cv::Mat ret;
	src.copyTo(ret);
	return ret;
}

int main(int argc, char *argv[]){

	auto img=CropToBigestEllipse(cv::imread("../P_20221023_133952_vHDR_On.jpg",cv::IMREAD_GRAYSCALE));
	cv::resize(img,img,cv::Size(),0.3,0.3);

	aze(cv::imread("../reference.png",cv::IMREAD_GRAYSCALE),img);
}


#include <iostream>
#include <stdio.h>
#include "opencv2/core.hpp"
#include "opencv2/core/utility.hpp"
#include "opencv2/core/ocl.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/calib3d.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/flann.hpp"
#include "opencv2/ml.hpp"
#include "opencv2/xfeatures2d.hpp"


void aze(cv::Mat ref,cv::Mat scene) {
	struct HomographyFinder{
		struct Homography{
			friend HomographyFinder;
			cv::Mat apply(const cv::Mat& img,const cv::Mat& refForSizeInfo){
				cv::Mat ret;
				cv::warpPerspective(img,ret,homography,cv::Size(refForSizeInfo.cols,refForSizeInfo.rows));
				return ret;
			}

			Homography operator *(const Homography& other){
				std::vector<cv::Point2f> src,intermediate,dst;
				src=/*a random square in space*/{
					cv::Point2f(0,0),
					cv::Point2f(1000,0),
					cv::Point2f(0,1000),
					cv::Point2f(1000,1000),
				};

				cv::perspectiveTransform(src, intermediate, this->homography);
				cv::perspectiveTransform(intermediate, dst, other.homography);

				Homography ret;
				ret.homography = cv::getPerspectiveTransform(src,dst);
				return ret;
			}

		private:
			cv::Mat homography;
		};

		HomographyFinder(cv::Mat ref,cv::Mat scene):ref(ref.clone()),scene(scene.clone()){
			refCorners={
				cv::Point(0, 0),
				cv::Point(ref.cols, 0),
				cv::Point(ref.cols, ref.rows),
				cv::Point(0, ref.rows)
			};
		}

		size_t findORBMatches(){
			return findFeatueMatches(cv::ORB::create(500));
		}
		size_t findSURFMatches(){
			return findFeatueMatches(cv::xfeatures2d::SURF::create(500));
		}


		size_t findFeatueMatches(std::shared_ptr<cv::Feature2D> featureDetector){
			//	auto featureDetector=cv::ORB::create(800);
			//	auto featureDetector=cv::xfeatures2d::SIFT::create(500);
			//	auto featureDetector=cv::xfeatures2d::SURF::create(500/*var*/);
			cv::Mat descriptorRef, descriptorScene;
			featureDetector -> detectAndCompute(ref, cv::Mat(), featueMatches.keyRef, descriptorRef);
			featureDetector -> detectAndCompute(scene, cv::Mat(), featueMatches.keyScene, descriptorScene);
			cv::BFMatcher().match(descriptorRef, descriptorScene, featueMatches.unfilteredSortedMatches);
			sort(featueMatches.unfilteredSortedMatches.begin(), featueMatches.unfilteredSortedMatches.end());

			return featueMatches.unfilteredSortedMatches.size();
		}

		void computeHomographies(double ratio=0.25){
			auto filteredMatches=featueMatches.unfilteredSortedMatches;
			filteredMatches.resize(filteredMatches.size() * ratio);

			std::vector<cv::Point2f> ptInRef,ptInScene;

			for (size_t i = 0; i < filteredMatches.size(); i++) {
				ptInRef.push_back(featueMatches.keyRef[filteredMatches[i].queryIdx].pt);
				ptInScene.push_back(featueMatches.keyScene[filteredMatches[i].trainIdx].pt);
			}

			ref2scene_.homography = cv::findHomography(ptInRef, ptInScene, cv::RANSAC);

			std::vector<cv::Point2f> sceneCorners;
			cv::perspectiveTransform(refCorners, sceneCorners, ref2scene_.homography);
			scene2ref_.homography = cv::getPerspectiveTransform(sceneCorners,refCorners);
		}


		cv::Mat utilsDrawMatches(double ratio=0.25){
			auto filteredMatches=featueMatches.unfilteredSortedMatches;
			filteredMatches.resize(filteredMatches.size() * ratio);

			cv::Mat outimg;
			cv::drawMatches(ref, featueMatches.keyRef, scene, featueMatches.keyScene, filteredMatches, outimg,
								 cv::Scalar::all(-1), cv::Scalar::all(-1),
								 std::vector<char>(),
								 cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS
								 );
			return outimg;
		}

		cv::Mat& utilsDrawPerspectiveOnMatches(cv::Mat& matchesDrawn){
			std::vector<cv::Point2f> sceneCorners;
			cv::perspectiveTransform(refCorners, sceneCorners, ref2scene_.homography);
			for (int i = 0; i < sceneCorners.size(); ++i) {
				cv::line(matchesDrawn, sceneCorners[i] + cv::Point2f(ref.cols, 0), sceneCorners[(i+1)%4] + cv::Point2f(ref.cols, 0), cv::Scalar(0, 255, 0), 2, cv::LINE_AA);
			}
			return matchesDrawn;
		}

		Homography ref2scene(){return ref2scene_;}
		Homography scene2ref(){return scene2ref_;}
	private:
		cv::Mat ref,scene;//!< set in constructor
		std::vector<cv::Point2f> refCorners;//!< set in constructor

		Homography ref2scene_, scene2ref_;//!< computed by ::computeHomographies()

		struct FeatueMatchesData{
			std::vector<cv::DMatch> unfilteredSortedMatches;
			std::vector<cv::KeyPoint> keyRef, keyScene;
		}featueMatches;
	};

	cv::Mat evolvingOut;
	auto show=[&evolvingOut](){
		cv::imshow("",evolvingOut);
		cv::waitKey();
	};


	HomographyFinder hf1(ref,scene);
	hf1.findSURFMatches();
	hf1.computeHomographies();

	evolvingOut=hf1.utilsDrawMatches();
	cv::imshow("",hf1.utilsDrawPerspectiveOnMatches(evolvingOut));
	cv::waitKey();

	evolvingOut=hf1.scene2ref().apply(scene,ref);
	show();

	HomographyFinder hf2(ref,evolvingOut);
	hf2.findSURFMatches();
	hf2.computeHomographies();

	evolvingOut=hf2.utilsDrawMatches();
	cv::imshow("",hf2.utilsDrawPerspectiveOnMatches(evolvingOut));
	cv::waitKey();

	evolvingOut=(hf1.scene2ref()*hf2.scene2ref()).apply(scene,ref);
	show();

	cv::imwrite("../output.png",evolvingOut);

//	cv::GaussianBlur(ref,ref,cv::Size(21,21),5,5);
//	cv::GaussianBlur(evolvingOut,evolvingOut,cv::Size(21,21),5,5);

	auto luminaceRatio=double(ref.at<uint8_t>(40,40))/evolvingOut.at<uint8_t>(40,40);

	evolvingOut*=luminaceRatio;

	cv::absdiff(ref,evolvingOut,evolvingOut);
	evolvingOut*=3;
	show();


	cv::multiply(cv::imread("../mask.png",cv::IMREAD_GRAYSCALE)/255.,evolvingOut,evolvingOut);
	show();

	return;
}
